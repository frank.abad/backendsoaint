Pasos para ejecutar la aplicacion
1. Se debe de restaurar el backup de la base de datos "prueba" esta en postgreSQL 13, encuentra el archivo prueba.sql
2. Se configura los parametros de la base de datos en la clase Principal
3. Asegurarse de tener unidad D: para guardar el archivo log sino cambiar la propiedad dbParam.put("logFileFolder","D:/");
4. Agregar en las librerias el archivo "postgresql-42.2.18.jar"
5. Ejecutar la clase Principal.
