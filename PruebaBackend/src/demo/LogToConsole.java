package demo;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogToConsole {
	private static Logger logger = Logger.getLogger("MyLog");
	
	public static void grabarLog(String messageText) {
		try {
			ConsoleHandler ch = new ConsoleHandler();
			logger.addHandler(ch);
	        logger.log(Level.INFO, messageText);
	        ch.close();
		}catch(Exception ex) {
			logger.log(Level.INFO, ex.getMessage());
		}
		
	}
	
}
