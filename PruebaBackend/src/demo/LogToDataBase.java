package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogToDataBase {

	private static Logger logger = Logger.getLogger("MyLog");
	
	public static void grabarLog(Map dbParams,String messageText, int tipo)  {
		try {
			Connection connection = null;
	        Properties connectionProps = new Properties();
	        connectionProps.put("user", dbParams.get("userName"));
	        connectionProps.put("password", dbParams.get("password"));

	        connection = DriverManager.getConnection("jdbc:" + dbParams.get("dbms") + "://" + dbParams.get("serverName")
	                + ":" + dbParams.get("portNumber") + "/"+dbParams.get("nameDatabase"), connectionProps);
	        
	        Statement stmt = connection.createStatement();
	        stmt.executeUpdate("INSERT INTO LOG_VALUES (mensaje, tipo) VALUES ('" + messageText + "', '" + String.valueOf(tipo) + "')");
	        connection.close();
		}catch(Exception ex) {
			logger.log(Level.INFO, ex.getMessage());
		}
		
	}
	
}
