package demo;

import java.io.File;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogToFile {

	private static Logger logger = Logger.getLogger("MyLog");
	
	public static void grabarLog(String logFileFolder, String messageText) {
		try {
			File logFile = new File(logFileFolder + "/logFile.txt");
	        if (!logFile.exists()) {
	            logFile.createNewFile();
	        }
	        FileHandler fh = new FileHandler(logFileFolder + "/logFile.txt");
	        logger.addHandler(fh);
	        logger.log(Level.INFO, messageText);
	        fh.close();
		}catch(Exception ex) {
			logger.log(Level.INFO, ex.getMessage());
		}
		
	}
}
