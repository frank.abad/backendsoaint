package demo;

import java.io.File;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Demo {
	private static boolean logToFile;
    private static boolean logToConsole;
    private static boolean logMessage;
    private static boolean logWarning;
    private static boolean logError;
    private static boolean logToDatabase;
    private boolean initialized;
    private static Map dbParams;
    private static Logger logger;

    public Demo(boolean logToFileParam, boolean logToConsoleParam, boolean logToDatabaseParam,
                boolean logMessageParam, boolean logWarningParam, boolean logErrorParam, Map dbParamsMap) {
        logger = Logger.getLogger("MyLog");
        logError = logErrorParam;
        logMessage = logMessageParam;
        logWarning = logWarningParam;
        logToDatabase = logToDatabaseParam;
        logToFile = logToFileParam;
        logToConsole = logToConsoleParam;
        dbParams = dbParamsMap;
    }
    
    public void LogMessage(String messageText, boolean message, boolean warning, boolean error) throws Exception {
    	int t = 0;
    	String l = new String("");
    	
    	if (messageText == null || messageText.trim().length() == 0) {
	        return;
	    }
	
	    if (!logToConsole && !logToFile && !logToDatabase) {
	        throw new Exception("Invalid configuration");
	    }
	    if ((!logError && !logMessage && !logWarning) || (!message && !warning && !error)) {
	        throw new Exception("Error or Warning or Message must be specified");
	    }

	    
	    if (message && logMessage) {
	        t = 1;
	    }
	
	    if (error && logError) {
	        t = 2;
	    }
	
	    if (warning && logWarning) {
	        t = 3;
	    }
	
	    
	    if (error && logError) {
	        l = l + "error " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
	    }
	
	    if (warning && logWarning) {
	        l = l + "warning " +DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
	    }
	
	    if (message && logMessage) {
	        l = l + "message " +DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
	    }
	
	    if(logToFile) {
	        LogToFile.grabarLog(dbParams.get("logFileFolder").toString(), messageText);
	    }
	
	    if(logToConsole) {
	    	LogToConsole.grabarLog(messageText);
	    }
	
	    if(logToDatabase) {
	       LogToDataBase.grabarLog(dbParams, l, t);
	    }
    }
}
