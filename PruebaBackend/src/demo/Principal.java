package demo;

import java.util.HashMap;
import java.util.Map;

public class Principal {
	public static void main(String []args) {
		Map<String, String> dbParam = new HashMap<String, String>();
		dbParam.put("dbms","postgresql");
		dbParam.put("serverName","127.0.0.1");
		dbParam.put("portNumber","5432");
		dbParam.put("userName","postgres");
		dbParam.put("password","qwerty");
		dbParam.put("nameDatabase", "prueba");
		dbParam.put("logFileFolder","D:/");
		Demo d = new Demo(true, true, true,true, true,true,dbParam);
		try {
			String cadena = "Hola Mundo";
			d.LogMessage(cadena, true, true, false);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
